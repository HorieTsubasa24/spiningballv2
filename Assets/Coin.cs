﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {
    public EffekseerEmitter effect;
    private MeshRenderer renderer;

    private void Start() {
        renderer = GetComponent<MeshRenderer>();
        EffekseerSystem.LoadEffect("ring");
    }

    // Update is called once per frame
    void Update () {
        transform.Rotate(0, 1.5f, 0);
	}

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Ball") {
            if (effect != null)
                effect.Play();
            Destroy(renderer);
            Destroy(gameObject, 1.0f);
        }
    }
}
