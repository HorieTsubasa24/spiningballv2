﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncBall : MonoBehaviour {
    public Transform ball;
    public Transform shp;
    public Transform cam;
    public Vector3 basecam;
    public float xrot;
    public Vector3 ballDiff;
    public float ballcammagnitute;
    private void Start() {
        basecam = ball.transform.position - cam.transform.position;
    }

    private void SyncBallDir() {
        var forword = ball.transform.forward;
        cam.position = new Vector3(basecam.x * -forword.x, basecam.y * -forword.y, basecam.z * -forword.z);
    }

    private void Update() {
        ball.position = shp.position;
        cam.rotation = Quaternion.Euler(xrot, ball.eulerAngles.y, ball.eulerAngles.z);
        //SyncBallDir();
    }
    
}
