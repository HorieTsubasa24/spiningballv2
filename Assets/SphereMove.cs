﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereMove : MonoBehaviour {
    public bool isGround;
    public Rigidbody regid;
    public Camera cam;
    public Transform ball;
    public Vector3 moveVec;
    public Vector3 direction;
    public Vector3 directionBefore;
    public Vector3 before;
    public float degree;
    public float mag = 3000;
    public float jumpmag = 50;

    public void Start() {
        regid = GetComponent<Rigidbody>();
        EffekseerSystem.LoadEffect("sonic");
    }

    public EffekseerEmitter jumpeffect;
    private void Jump() {
        if (inJump && isGround) {
            regid.AddForce(mag * Vector3.up * jumpmag / 4);
            jumpeffect.Play();
        }
    }

    private void ToForward() {
        Vector3 momentAddition = ball.transform.forward;
        if (isGround == true) {
            regid.AddForce(momentAddition * inY * mag);
        } else {
            regid.AddForce(momentAddition * inY * mag * 0.5f);
        }
    }

    private void RunnningEffect() {
        if (isGround == false)
            return;

    }

    public void Gravity2ndOnFly() {
        if (isGround == false)
            regid.AddForce(Vector3.down * jumpmag * 8);
    }

    public void Update() {
        ball.Rotate(Vector3.up * 5f * inX);
        ToForward();
        Jump();
        RunnningEffect();
        Gravity2ndOnFly();
    }

    public float inX;
    public float inY;
    public bool inJumpBef;
    public bool inJump;
    public void LateUpdate() {
        inX = Input.GetAxis("Horizontal");
        inY = Input.GetAxis("Vertical");
        inJump = Input.GetButton("Jump");
        if (inJumpBef == true && inJump == true) {
            inJump = false;
            return;
        }
        inJumpBef = inJump;
    }
}
